.TH HILIVE "1" "October 2019" "hilive 2.0a" "User Commands"
.SH NAME
hilive \- Realtime Alignment of Illumina Reads
.SH SYNOPSIS
.B hilive
[options]
.SH DESCRIPTION
HiLive is a read mapping tool that maps Illumina HiSeq (or comparable)
reads to a reference genome right in the moment when they are produced.
This means, read mapping is finished as soon as the sequencer is
finished generating the data.
.SH OPTIONS
.SS "REQUIRED OPTIONS:"
.TP
\fB\-b\fR [\-\-bcl\-dir]
Illumina's BaseCalls directory which contains the sequence information of the reads.
.TP
\fB\-i\fR [\-\-index]
Path to the HiLive index.
.TP
\fB\-r\fR [\-\-reads]
Length and types of the read segments.
.PP
Required options might be specified either on the command line or in the config file.
.SS "GENERAL OPTIONS:"
.TP
\fB\-h\fR [ \fB\-\-help\fR ]
Print this help message and exit.
.TP
\fB\-l\fR [ \fB\-\-license\fR ]
Print license information and exit.
.TP
\fB\-c\fR [ \fB\-\-config\fR ] arg
Path to a config file. Config file is in
\&.ini format. Duplicate keys are not
permitted. Instead, use comma\-separated
lists. Parameters obtained from the command
line are prioritized over settings made in
the config file.
.TP
Example for a config.ini:
bcl\-dir=./BaseCalls
lanes=1
out\-cycle=50,100
.TP
\fB\-\-runinfo\fR arg
Path to Illumina's runInfo.xml file. If
specified, read lengths, lane count and
tile count are automatically set in
accordance with the sequencing run.
Parameters obtained from the command line
or config file are prioritized over
settings obtained from the runInfo.xml.
.TP
\fB\-\-continue\fR arg
Continue an interrupted HiLive run from a
specified cycle. We strongly recommend to
load the config file that was automatically
created for the original run to continue
with identical settings. This config file
(hilive_config.ini) can be found in the
temporary directory specified with
\fB\-\-temp\-dir\fR.
.SS "SEQUENCING OPTIONS:"
.TP
\fB\-b\fR [ \fB\-\-bcl\-dir\fR ] arg
Illumina's BaseCalls directory which
contains the sequence information of the
reads.
.TP
\fB\-l\fR [ \fB\-\-lanes\fR ] arg
Specify the lanes to be considered for read
alignment. [Default: 1\-8]
.TP
\fB\-t\fR [ \fB\-\-tiles\fR ] arg
Specify the tiles to be considered for read
alignment. [Default: [1\-2][1\-3][01\-16] (96
tiles)]
.TP
\fB\-T\fR [ \fB\-\-max\-tile\fR ] arg
Specify the highest tile number. The tile
numbers will be computed by this number,
considering the correct surface count,
swath count and tile count for Illumina
sequencing.
This parameter serves as a shortcut for
\fB\-\-tiles\fR.
.TP
Example:
\fB\-\-max\-tile\fR 2216
.TP
will activate all tiles in
[1\-2][1\-2][01\-16].
.TP
\fB\-r\fR [ \fB\-\-reads\fR ] arg
Length and types of the read segments. Each
segment is either a read ('R') or a barcode
('B'). Please give the segments in the
correct order as they are produced by the
sequencing machine. [REQUIRED]
.TP
Example:
\fB\-\-reads\fR 101R,8B,8B,101R
.TP
specifies paired\-end sequencing with
2x101bp reads and 2x8bp barcodes.
.TP
\fB\-B\fR [ \fB\-\-barcodes\fR ] arg
Barcode(s) of the sample(s) to be
considered for read alignment. Barcodes
must match the barcode length(s) as
specified with \fB\-\-reads\fR. Delimit different
segments of the same barcodes by '\-' and
different barcodes by ','. [Default: All
barcodes]
.TP
Example:
\fB\-b\fR ACCG\-ATTG,ATGT\-TGAC
.IP
for two different barcodes of length 2x4bp.
.TP
\fB\-\-run\-id\fR arg
ID of the sequencing run. Should be
obtained from runInfo.xml.
.TP
\fB\-\-flowcell\-id\fR arg
ID of the flowcell. Should be obtained from
runInfo.xml.
.TP
\fB\-\-instrument\-id\fR arg
ID of the sequencing machine. Should be
obtained from runInfo.xml.
.SS "REPORT OPTIONS:"
.TP
\fB\-o\fR [ \fB\-\-out\-dir\fR ] arg
Path to the directory that is used for the
output files. The directory will be created
if it does not exist. [Default: ./out]
.TP
\fB\-f\fR [ \fB\-\-out\-format\fR ] arg
Format of the output files. Currently, SAM
and BAM format are supported. [Default:
BAM]
.TP
\fB\-O\fR [ \fB\-\-out\-cycles\fR ] arg
Cycles for that alignment output is
written. The respective temporary files are
kept. [Default: write only after the last
cycle]
.TP
\fB\-M\fR [ \fB\-\-out\-mode\fR ] arg
The output mode. [Default: ANYBEST]
[ALL|A]: Report all found alignments.
[BESTN#|N#]: Report the # best found
alignments.
[ALLBEST|H]: Report all found alignments
with the best score.
[ANYBEST|B]: Report one best alignment.
[UNIQUE|U]: Report only unique alignments.
.TP
\fB\-\-report\-unmapped\fR
Activate reporting unmapped reads.
[Default: false]
.TP
\fB\-\-extended\-cigar\fR
Activate extended CIGAR format for the
alignment output files ('=' for matches and
\&'X' for mismatches instead of using 'M' for
both). [Default: false]
.TP
\fB\-\-force\-resort\fR
Always sort temporary alignment files
before writing output. Existing sorted
align files are overwritten. This is only
necessary if the temp directory is used
more than once for new alignments. In
general, this is not recommended for most
applications. [Default: false (only sort if
no sorted files exist)]
.TP
\fB\-\-max\-softclip\-ratio\fR arg
Maximal relative length of the front
softclip (only relevant during output)
[Default: 0.2]
.TP
Further explanation:
HiLive uses an approach that requires one
exact match of a k\-mer at the beginning of
an alignment. This can lead to unaligned
regions at the beginning of the read which
we report as 'softclips'. With this
parameter, you can control the maximal
length of this region.
.SS "ALIGNMENT OPTIONS:"
.TP
\fB\-i\fR [ \fB\-\-index\fR ] arg
Path to the HiLive index. Please use the
executable 'hilive\-build' to create a new
HiLive index that is delivered with this
program. The index consists of several
files with the same prefix. Please include
the file prefix when specifying the index
location.
.TP
\fB\-m\fR [ \fB\-\-align\-mode\fR ] arg
Alignment mode to balance speed and
accuracy [very\-fast|fast|balanced|accurate|
very\-accurate]. This selected mode
automatically sets other parameters.
Individually configured parameters are
prioritized over settings made by selecting
an alignment mode. [Default: balanced]
.TP
\fB\-\-anchor\-length\fR arg
Length of the alignment anchor (or initial
seed) [Default: set by the selected
alignment mode]
.TP
\fB\-\-error\-interval\fR arg
The interval to tolerate more errors during
alignment (low=accurate; great=fast).
[Default: 'anchor\-length'/2]
.TP
\fB\-\-seeding\-interval\fR arg
The interval to create new seeds
(low=accurate; great=fast). [Default:
\&'anchor\-length'/2]
.TP
\fB\-\-max\-softclip\-length\fR arg
The maximum length of a front softclip when
creating new seeds. In contrast to
\fB\-\-max\-softclip\-ratio\fR, this parameter may
have effects on runtime and mapping
accuracy. [Default: 'readlength/2]
.TP
\fB\-\-barcode\-errors\fR arg
The number of errors that are tolerated for
the barcode segments. A single value can be
provided to be applied for all barcode
segments. Alternatively, the value can be
set for each segment individually.
[Default: 1]
.TP
Example:
\fB\-\-barcode\-errors\fR 2 [2 errors for all barcode segments]
.IP
\fB\-\-barcode\-errors\fR 2,1 [2 errors for the first, 1 error for the second segment]
.TP
\fB\-\-align\-undetermined\-barcodes\fR
Align all barcodes. Reads with a barcode
that don't match one of the barcodes
specified with '\-\-barcodes' will be
reported as undetermined. [Default: false]
.TP
\fB\-\-min\-basecall\-quality\fR arg
Minimum basecall quality for a nucleotide
to be considered as a match [Default: 1
(everything but N\-calls)]
.TP
\fB\-\-keep\-invalid\-sequences\fR
Keep sequences of invalid reads, i.e. with
unconsidered barcode or filtered by the
sequencer. This option must be activated to
report unmapped reads. [Default: false]
.SS "SCORING OPTIONS:"
.TP
\fB\-s\fR [ \fB\-\-min\-as\fR ] arg
Minimum alignment score. [Default: Set
automatically based on the alignment mode
and match/mismatch scores]
.TP
\fB\-\-match\-score\fR arg
Score for a match. [Default: 0]
.TP
\fB\-\-mismatch\-penalty\fR arg
Penalty for a mismatch. [Default: 6]
.TP
\fB\-\-insertion\-opening\-penalty\fR arg
Penalty for insertion opening. [Default: 5]
.TP
\fB\-\-insertion\-extension\-penalty\fR arg Penalty for insertion extension. [Default: 3]
.TP
\fB\-\-deletion\-opening\-penalty\fR arg
Penalty for deletion opening. [Default: 5]
.TP
\fB\-\-deletion\-extension\-penalty\fR arg
Penalty for deletion extension. [Default:
3]
.TP
\fB\-\-max\-gap\-length\fR arg
Maximal permitted consecutive gap length.
Increasing this parameter may lead to
highly increased runtime! [Default: 3]
.TP
\fB\-\-softclip\-opening\-penalty\fR arg
Penalty for softclip opening (only relevant
during output). [Default:
\&'mismatch\-penalty']
.TP
\fB\-\-softclip\-extension\-penalty\fR arg
Penalty for softclip extension (only
relevant during output). [Default:
\&'mismatch\-penalty'/'anchor\-length']
.SS "TECHNICAL OPTIONS:"
.TP
\fB\-\-temp\-dir\fR arg
Temporary directory to store the alignment
files and hilive_config.ini. [Default:
\&./temp]
.TP
\fB\-k\fR [ \fB\-\-keep\-files\fR ] arg
Keep intermediate alignment files for these
cycles. The last cycle is always kept.
[Default: Keep files of output cycles]
.TP
Further Explanations:
HiLive comes with a separated executable
\&'hilive\-out'. This executable can be used
to produce alignment files in SAM or BAM
format from existing temporary files. Thus,
output can only be created for cycles for
that keeping the temporary alignment files
is activated. Temporary alignment files are
also needed if an interrupted run is
continued with the '\-\-continue' parameter.
.TP
\fB\-K\fR [ \fB\-\-keep\-all\-files\fR ]
Keep all intermediate alignment files. This
option may lead to huge disk space
requirements. [Default: false]
.TP
\fB\-\-block\-size\fR arg
Block size for the alignment input/output
stream in Bytes. Append 'K' or 'M' to
specify in Kilobytes or Megabytes,
respectively. [Default: 64M]
.TP
Example:
\fB\-\-block\-size\fR 1024 [1024 bytes]
\fB\-\-block\-size\fR 64K [64 Kilobytes]
\fB\-\-block\-size\fR 64M [64 Megabytes]
.TP
\fB\-\-compression\fR arg
Compression of temporary alignment files.
[Default: LZ4]
0: no compression.
1: Deflate (smaller).
2: LZ4 (faster).
.TP
\fB\-n\fR [ \fB\-\-num\-threads\fR ] arg
Number of threads to spawn (including
output threads). [Default: 1]
.TP
\fB\-N\fR [ \fB\-\-num\-out\-threads\fR ] arg
Maximum number of threads to use for
output. More threads may be used for output
automatically if threads are idle.
[Default: 'num\-threads'/2]
.SH EXAMPLE
hilive \fB\-\-bcl\-dir\fR ./BaseCalls \fB\-\-index\fR ./reference/hg19 \fB\-\-reads\fR 101R
.SH AUTHOR
This manpage was written by Andreas Tille for the Debian distribution and can be used for any other usage of the program.
